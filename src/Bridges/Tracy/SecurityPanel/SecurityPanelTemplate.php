<?php
declare(strict_types=1);
namespace Krekos\Security\Bridges\Tracy\SecurityPanel;

use Krekos\Security\Authentication\Firewall;

final class SecurityPanelTemplate{

    /** @var Firewall[] */
    public array $firewalls;
}