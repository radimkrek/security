<?php
declare(strict_types=1);

namespace Krekos\Security\Authentication;

interface UserStorage extends \Nette\Security\UserStorage{

	public function setNamespace(string $namespace):void;
}