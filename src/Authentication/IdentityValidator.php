<?php
declare(strict_types=1);

namespace Krekos\Security\Authentication;

use Nette\Security\IIdentity;

interface IdentityValidator{

	public function validate(IIdentity $identity):IIdentity;
}