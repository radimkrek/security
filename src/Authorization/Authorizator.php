<?php
declare(strict_types=1);

namespace Krekos\Security\Authorization;

/**
 * @author Jáchym Toušek <enumag@gmail.com>
 */
interface Authorizator{

	/**
	 * @param \Nette\Security\Resource|string $resource
	 */
	public function isAllowed($resource, string $privilege):bool;
}
